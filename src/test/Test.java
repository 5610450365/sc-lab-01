package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import model.Card;
import gui.GUI;

public class Test {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Test();
	}

	public Test() {
		frame = new GUI();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		/*
		 * Student may modify this code and write your result here
		 * frame.setResult("aaa\t bbb\t ccc\t"); frame.extendResult("ddd");
		 */
		Card stu1 = new Card(50969);
		stu1.setName("Nawara Lersiripong");
		stu1.setOrdername("Orange");
		stu1.setPrice(1000);
		stu1.Dep(300);
		stu1.Total();
		stu1.Withdraw(150);
		frame.setResult(stu1.toString());
		

	}

	ActionListener list;
	GUI frame;
}

